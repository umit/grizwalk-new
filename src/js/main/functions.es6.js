var request = require("-aek/request");

export function requestCallMainCategories(path, position) {
    var main_category_array = [];

    // Loads the sub-category checkboxes. Gets server side request in .ect file to start and returns in a call back.

    if (position != null) {

        request.get(path)
            .query(
                {
                    format: "json",
                    latitude: position.coords.latitude,//[position.coords.latitude, position.coords.longitude]
                    longitude: position.coords.longitude
                })
            .end((err, res)=> {

                // Checks if there's no error, but a response has been recieved.
                if (!err && res) {
                    //console.log("response", res.body.locations.original[0]);
                    //console.log("position", position);
                    for (var i = 0; i < res.body.locations.original.length; i++) {
                        main_category_array.push(res.body.locations.original[i]);
                    }
                }
            });

    }

    else {

        request.get(path)
            .query(
                {
                    format: "json",

                })
            .end((err, res)=> {

                // Checks if there's no error, but a response has been recieved.
                if (!err && res) {
                    //console.log("response", res.text);
                    //console.log("position", position);
                    for (var i = 0; i < res.body.locations.original.length; i++) {
                        main_category_array.push(res.body.locations.original[i]);
                    }
                }
            });
    }
    return main_category_array;
}
