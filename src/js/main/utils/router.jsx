var React = require("-aek/react");
var {AekReactRouter} = require("-components/router");
var {Panel} = require("-components/layout");
var Page = require("-components/page");

var router = new AekReactRouter(
    {
        defaultHandler:React.createClass(
            {
                componentWillMount: function()
                {
                    setTimeout(function()
                    {
                        router.goto('/');
                    },1);
                },
                render:function() {return (<Page><Panel loading={true} /></Page>);}
            })
    });

module.exports = router;