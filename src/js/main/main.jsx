var React = require("-aek/react");
var Container = require("-components/container");
var {VBox, Panel} = require("-components/layout");
var {BannerHeader} = require("-components/header");
var {BasicSegment, Segment} = require("-components/segment");
var request = require('-aek/request');
var router = require('./utils/router');
var {Listview, Item} = require("-components/listview");
var Form = require("@ombiel/aek-lib/react/components/form");
import {requestCallMainCategories} from "./functions";

var token = document.getElementById("cmUsername").textContent;
token = token.trim();
var hidden_netid = document.getElementById("cmUsername").textContent;
hidden_netid = hidden_netid.trim();

var pickups;
var destinations;
var position = null;
var fields = [];

function getCoords(callback) {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (response) {
                callback(response);
            }, function () {

                callback(null)
            },
            {timeout: 5000}
        )
    }
}

getCoords(function (result) {

    if (result != null) {
        position = result;
        pickups = requestCallMainCategories("https://appstest.umt.edu/grizwalk/index", position);
        destinations = pickups;
        pickups.unshift("Use my location");
    }

    else {

        pickups = requestCallMainCategories("https://appstest.umt.edu/grizwalk/index", null); //http://127.0.0.1:8000/index (localhost address)
        destinations = pickups;
    }

    fields = [

        {
            name: "Phone",
            label: "Phone number:",
            type: "text",
            placeholder: "Ex: 987654321",
            required: true
        },

        {
            name: "People",
            label: "Number of people:",
            type: "number",
            placeholder: "Ex: 4",
            required: true
        },

        {
            name: "Location",
            label: "Where are you?",
            type: "select",
            placeholder: "Choose a location",
            options: pickups,
            required: true
        },

        {
            name: "Destination",
            label: "Where are you going?",
            type: "select",
            placeholder: "Choose a location",
            options: destinations,
            required: true
        }


    ];


});








var Screen = React.createClass({

    getInitialState: function () {
        return {response: "", data: {}, error: null, form: {netid: hidden_netid, _token: token}};
    },

    componentDidMount: function () {
        setTimeout(() => this.setState({ loading: false }), 3000);
        this.getData();
    },

    getData: function () {
        /*request
            .get('https://appstest.umt.edu/mobile-analytics/user')
            .query({netid: hidden_netid})
            .query({key: 'Grizwalk'})
            .end();*/
    },

    onChange: function (e, name, value) {
        var form_data = _.extend({}, this.state.form, {[name]: value});
        this.setState({form: form_data, error: null});
    },

    _onSubmit: function (e) {
        e.preventDefault();
        $.when(this._create()).done(function () {
        });
    },






  render:function() {

      const { loading } = this.state;

      if(loading) { // if your app get render immediately, remove this block
          return null; // render null when app is not ready
      }

    return (
      <Container>
        <VBox>
          <Panel>
          <BannerHeader theme="alt" key="header" flex={0}>Grizwalk</BannerHeader>
          <BasicSegment>

              <p>GrizWalk is a service provided by the UM Police Department's GrizWalk Security Team.
                  Student teams provide safe walking or driving escorts anytime between dusk and dawn.
              </p>

              <p>If GrizWalk is unavailable, a UM Police Officer will provide the same service.</p>
            <div>
              <Segment>

                <Form method="POST" data={this.state.form} fields={fields}
                      onChange={this.onChange}/>
              </Segment>
              <div className="button-div">
                <button disabled={this.state.error} type="submit" onClick={this._onSubmit}> Order
                  Ride
                </button>
              </div>
            </div>
          </BasicSegment>
          </Panel>
        </VBox>
      </Container>
    );

  }

});

React.render(<Screen/>,document.body);
